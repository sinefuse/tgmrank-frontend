import 'regenerator-runtime/runtime';
import { en, fr, ja } from 'make-plural';
import path from 'path';

import express from 'express';
import cookieParser from 'cookie-parser';
import acceptLanguage from 'accept-language';

import React from 'react';
import { renderToString } from 'react-dom/server';
import { HelmetProvider } from 'react-helmet-async';
import { ServerLocation } from '@reach/router';
import { fetcher } from './src/api';

import App from './src/app';
import { i18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';

import BgLoginImage from './images/bg-login.jpg';
import BgLoginCardImage from './images/bg-login-card.jpg';
import TgmRank from './src/tgmrank-api';

import * as enMessages from './src/locale/en/messages.js';
import * as frMessages from './src/locale/fr/messages.js';
import * as jaMessages from './src/locale/ja/messages.js';

const server = express();
const port = process.env.PORT || 1234;

const buildManifest = require('./dist/manifest.json');

const script = buildManifest['main.js'];
const styles = buildManifest['main.css'];

const defaultLanguage = 'en';
acceptLanguage.languages(['en', 'fr', 'ja']);
const catalogs = {
  en: enMessages,
  fr: frMessages,
  ja: jaMessages,
};

server.use(
  express.static(path.resolve('./dist'), {
    immutable: true,
    maxAge: '365d',
  }),
);

server.use(cookieParser());

function detectLocale(req) {
  const { locale } = req.cookies;
  return (
    acceptLanguage.get(locale || req.headers['accept-language']) ||
    defaultLanguage
  );
}

server.disable('x-powered-by');

server.get('*', async (req, res) => {
  try {
    const tgmRankClient = new TgmRank(
      process.env.SERVER_TGMRANK_BASE_URL ?? process.env.TGMRANK_BASE_URL,
    );

    const ssrData = {
      // TODO: Centralize SSR fetch and client-side fetch
      useGames: await fetcher(tgmRankClient.getGamesUrl()),
    };

    const useGamesJsonString = JSON.stringify(ssrData.useGames);

    const locale = detectLocale(req);
    const catalog = catalogs[locale];
    delete catalog.defaults;

    i18n.loadLocaleData({
      en: { plurals: en },
      fr: { plurals: fr },
      ja: { plurals: ja },
    });

    i18n.load(locale, catalog.messages);
    i18n.activate(locale);

    const darkTheme = req.cookies['dark-theme'];
    res.cookie('locale', locale, {
      maxAge: new Date() * 0.001 + 365 * 24 * 3600,
      sameSite: 'strict',
      secure: !process.env.TGMRANK_BASE_URL.includes('localhost'),
    });
    const helmetContext = {};
    const app = renderToString(
      <I18nProvider i18n={i18n}>
        <ServerLocation url={req.path}>
          <HelmetProvider context={helmetContext}>
            <App ssrData={ssrData} />
          </HelmetProvider>
        </ServerLocation>
      </I18nProvider>,
    );
    const { helmet } = helmetContext;
    const helmetTitle = helmet.title.toComponent()?.[0]?.key ?? 'Placeholder';
    const frontendUrl = process.env.FRONTEND_URL ?? 'Placeholder';
    const description =
      'Leaderboards for the TGM (Tetris: The Grandmaster) Series of Arcade Games';
    const html = `
    <!doctype html>
    <html lang="${locale}">
        <head>
            ${helmet.title.toString()}
            <meta name="description" content="${description}">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="robots" content="index, follow">
            <link rel="stylesheet" href="${styles}">
            ${helmet.meta.toString()}
            ${helmet.link.toString()}
            <meta property="og:type" content="website"/>
            <meta property="og:url" content="${frontendUrl}"/>
            <meta property="og:title" content="${helmetTitle}"/>
            <meta property="og:description" content="${description}"/>
            <meta property="og:image" content="${frontendUrl}${BgLoginImage}">
            <meta property="twitter:card" content="summary"/>
            <meta property="twitter:url" content="${frontendUrl}"/>
            <meta property="twitter:text:title" content="${helmetTitle}"/>
            <meta property="twitter:title" content="${helmetTitle}"/>
            <meta property="twitter:description" content="${description}"/>
            <meta property="twitter:image" content="${frontendUrl}${BgLoginCardImage}">
        </head>
        <body class="${darkTheme === 'true' ? 'dark-theme' : ''}">
            <div id="root">
                ${app}
            </div>
            <script>window.__I18N_CATALOGS__ = {${locale}:${JSON.stringify(
      catalog,
    )}}; window.data = { useGames: ${useGamesJsonString}}</script>
            ${
              /*
              TODO Load up language catalog in here
            <script>window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
                  /</g,
                  '\\u003c',
                )}</script>
                */ ''
            }
            <script src="${script}"></script>
        </body>
    </html>
  `;

    res.status(200).send(html);
  } catch (error) {
    console.log(error);
  }
});

/**
 * Start server
 */
server.listen(port, () => console.log(`listening at http://localhost:${port}`));
