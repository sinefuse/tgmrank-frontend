import React from 'react';
import { Link as ReachLink } from '@reach/router';

export default function Link(props) {
  return <ReachLink onClick={event => event.stopPropagation()} {...props} />;
}
