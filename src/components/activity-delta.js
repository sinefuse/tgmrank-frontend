import React from 'react';
import { Trans, Plural } from '@lingui/macro';
import LeaderboardRank from './leaderboard/rank';
import { GameLink, ModeLink } from './mode-name';
import { useGames } from '../hooks/use-games';

function DeltaItem({ title, mode, delta }) {
  const previousPoints =
    delta.previousRank != null && delta.previousRankingPoints == null
      ? 0
      : delta.previousRankingPoints; // This is just because the API sometimes returns null when there was a previous rank but the user had no points.
  const pointRaise = delta.rankingPoints - previousPoints;

  // HACK: If this score is new to the leaderboard, you won't have a previousRank, but the recent activity response will tell you how many players you passed
  const rankRaise =
    delta.previousRank != null
      ? delta.previousRank - delta.rank
      : delta.playersOffset.length;

  const shouldShowPoints = mode == null || mode?.isRanked;

  return (
    <div>
      <h4>{title}</h4>
      {delta.rank != null ? (
        <>
          <div>
            <Trans>
              Ranked <LeaderboardRank rank={delta.rank} />
            </Trans>
            {rankRaise > 0 && (
              <span className="green">
                {' +'}
                {rankRaise}
              </span>
            )}
          </div>
          {shouldShowPoints && (
            <div>
              <Plural
                value={delta.rankingPoints}
                one="# point"
                other="# points"
              />
              {pointRaise > 0 && (
                <span className="green">
                  {' +'}
                  {pointRaise}
                </span>
              )}
            </div>
          )}
        </>
      ) : (
        <div>
          <Trans>Unranked</Trans>
        </div>
      )}
    </div>
  );
}

export default function ActivityDelta({ game, mode, delta }) {
  const { games } = useGames();

  return (
    <div className="activity__delta">
      <DeltaItem
        title={
          <GameLink game={games?.overall}>
            <Trans>Overall</Trans>
          </GameLink>
        }
        delta={delta.overall}
      />
      <DeltaItem
        title={
          <GameLink game={game}>
            <Trans>Game</Trans>
          </GameLink>
        }
        delta={delta.game}
      />
      <DeltaItem
        title={
          <ModeLink game={game} mode={mode}>
            <Trans>Mode</Trans>
          </ModeLink>
        }
        mode={mode}
        delta={delta.mode}
      />
    </div>
  );
}
