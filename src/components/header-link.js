import React from 'react';
import { Link } from '@reach/router';
import objstr from 'obj-str';

export default function NavLink(props) {
  return (
    <Link
      {...props}
      getProps={obj => {
        const isActive = obj.location.pathname
          .toLowerCase()
          .startsWith(obj.href.toLowerCase());

        return {
          className: objstr({
            'nav-link': true,
            'nav-link--active': isActive,
          }),
        };
      }}
    />
  );
}
