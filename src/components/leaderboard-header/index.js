import { useLingui } from '@lingui/react';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import useSWR from 'swr';
import { Trans } from '@lingui/macro';

import { positionRight } from '@reach/popover';

import {
  Menu,
  MenuButton,
  MenuItems,
  MenuLink,
  MenuPopover,
} from '@reach/menu-button';

import { Eye as EyeIcon, Twitch as TwitchIcon } from 'react-feather';
import { useGames } from '../../hooks/use-games';

import Dialog from '../dialog';
import { LeaderboardNavLink } from '../nav-link';
import ModeName from '../mode-name';

import overall from '../../../images/decoration/huge.png';
import tgm1 from '../../../images/game-icons/tgm.png';
import tap from '../../../images/game-icons/tap.png';
import ti from '../../../images/game-icons/ti.png';
import TgmRank from '../../tgmrank-api';

import './styles.css';
import { LeaderboardFilter } from './filter';
import { AggregateLeaderboardLabel } from '../leaderboard/label';

const icons = {
  overall,
  tgm1,
  tap,
  ti,
};

export function ModeDescription({ modeId, showDefault = true }) {
  const { data, error } = useSWR(
    modeId != null ? TgmRank.getModeDetailsUrl(modeId) : null,
  );

  const isLoading = !data && !error;
  if (isLoading) {
    return null;
  }

  return (
    <div style={{ whiteSpace: 'pre-line' }}>
      {data?.description ? (
        <ReactMarkdown>{data.description}</ReactMarkdown>
      ) : (
        showDefault && (
          <Trans>
            This mode does not have formal rules yet. Be cautious and use your
            best judgment :)
          </Trans>
        )
      )}
    </div>
  );
}

function StreamCount({ game }) {
  const { games } = useGames();
  const { data: streams } = useSWR(TgmRank.twitchStreamsInfoUrl());

  if (streams == null) {
    return null;
  }

  const gameStreams = streams?.filter(
    s => game.gameId === games.overall.gameId || s.gameId === game.gameId,
  );

  const streamCount = gameStreams?.length;

  return (
    <span className="ml-1">
      <TwitchIcon size={18} />
      <span>{streamCount}</span>
    </span>
  );
}

function ViewerCount({ game }) {
  const { games } = useGames();
  const { data: streams } = useSWR(TgmRank.twitchStreamsInfoUrl());

  if (streams == null) {
    return null;
  }

  const gameStreams = streams?.filter(
    s => game.gameId === games.overall.gameId || s.gameId === game.gameId,
  );

  const totalViewers = gameStreams?.reduce(
    (sum, stream) => sum + stream.viewerCount,
    0,
  );

  return (
    <span className="ml-1">
      <EyeIcon size={18} />
      <span>{totalViewers}</span>
    </span>
  );
}

export function LiveStreams({ game }) {
  return (
    <span className="ml-1">
      <StreamCount game={game} />
      <ViewerCount game={game} />
    </span>
  );
}

export function ModeDescriptionLink({ modeId, children }) {
  const [isDialogVisible, showDialog] = React.useState(false);

  if (modeId == null) {
    return null;
  }

  return (
    <>
      <a
        onClick={event => {
          event.stopPropagation();
          event.preventDefault();
          showDialog(true);
        }}
      >
        {children ?? <Trans>View Rules</Trans>}
      </a>
      <Dialog
        aria-label="Leaderboard Rules"
        isOpen={isDialogVisible}
        close={() => showDialog(false)}
      >
        <ModeDescription modeId={modeId} />
      </Dialog>
    </>
  );
}

function MenuLinkTarget({ ...props }) {
  const isExternal = props.href.startsWith('http');
  return (
    <MenuLink
      {...props}
      target={isExternal ? '_blank' : '_self'}
      rel={isExternal ? 'noopener noreferrer' : ''}
    />
  );
}

function ExtraLinks({ game, currentMode }) {
  const extraModeLinks =
    currentMode?.links?.map((link, index) => (
      <MenuLinkTarget key={`extra${index}`} href={link.link}>
        {link.title}
      </MenuLinkTarget>
    )) ?? [];

  const menuItems = [
    game?.wikiLink && (
      <MenuLinkTarget
        key={'wiki'}
        href={game.wikiLink}
        target="_blank"
        rel="noopener noreferrer"
      >
        <Trans>Game Info</Trans>
      </MenuLinkTarget>
    ),
    game?.twitchLink && (
      <MenuLinkTarget key={'twitch'} href={game.twitchLink}>
        <Trans>Twitch</Trans>
      </MenuLinkTarget>
    ),
    currentMode?.link && (
      <MenuLinkTarget key={'legacy'} href={currentMode.link}>
        <Trans>Legacy Leaderboard</Trans>
      </MenuLinkTarget>
    ),
    ...extraModeLinks,
  ].filter(i => i != null);

  return (
    menuItems.length > 0 && (
      <Menu>
        <MenuButton>&#10247;</MenuButton>
        <MenuPopover position={positionRight}>
          <MenuItems>{menuItems}</MenuItems>
        </MenuPopover>
      </Menu>
    )
  );
}

export function LeaderboardNavigation({ game }) {
  if (game == null) {
    return null;
  }
  return (
    <nav className="nav nav--modes">
      {Object.entries(game.rankedModes).map(([aggLeaderboardKey]) => (
        <LeaderboardNavLink
          key={aggLeaderboardKey}
          data-bold-toggle={aggLeaderboardKey}
          game={game}
          rankedModeKey={aggLeaderboardKey}
        >
          <AggregateLeaderboardLabel
            game={game}
            rankedModeKey={aggLeaderboardKey}
          />
        </LeaderboardNavLink>
      ))}
      {game?.modes
        ?.filter(m => m.isActive() || m.isRecent())
        .map(mode => (
          <LeaderboardNavLink
            key={mode.modeId}
            data-bold-toggle={mode.modeName}
            game={game}
            mode={mode}
          >
            <ModeName {...mode} />
          </LeaderboardNavLink>
        ))}
    </nav>
  );
}

export default function BoardHeader({ currentGame, currentMode }) {
  const { i18n } = useLingui();
  return (
    <>
      <div className="board-header">
        <img
          src={icons[currentGame?.shortName.toLowerCase()]}
          className="board-header__icon"
        />
        <div className="board-header__title">
          {currentGame?.gameName ? (
            <h1>{i18n._(currentGame?.gameName)}</h1>
          ) : (
            <h1 />
          )}
          {currentGame?.subtitle ? (
            <h3 className="jp">{currentGame?.subtitle}</h3>
          ) : (
            <h3 />
          )}
        </div>
        <div className="board-header__links">
          {currentGame?.twitchLink ? (
            <a
              href={currentGame?.twitchLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              <LiveStreams game={currentGame} />
            </a>
          ) : (
            <LiveStreams game={currentGame} />
          )}
          <LeaderboardFilter />
          <div className="board-header__button">
            {currentMode && (
              <ModeDescriptionLink modeId={currentMode?.modeId} />
            )}
          </div>
          <div className="board-header__overflow-menu">
            <ExtraLinks game={currentGame} currentMode={currentMode} />
          </div>
        </div>
      </div>
    </>
  );
}
