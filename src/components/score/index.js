import React from 'react';
import { Trans } from '@lingui/macro';
import { ScoreStatus } from '../../tgmrank-api';
import { toTitleCase } from '../string-utilities';
import {
  HelpCircle as HelpCircleIcon,
  Book as BookIcon,
  Clock as ClockIcon,
  X as XIcon,
  Check as CheckIcon,
} from 'react-feather';

export function ScoreStatusIcon({ status, size }) {
  const scoreStatusIcons = {
    [ScoreStatus.Unverified]: <HelpCircleIcon size={size} />,
    [ScoreStatus.Pending]: <ClockIcon size={size} />,
    [ScoreStatus.Legacy]: <BookIcon size={size} />,
    [ScoreStatus.Rejected]: <XIcon size={size} />,
    [ScoreStatus.Verified]: <CheckIcon size={size} />,
    [ScoreStatus.Accepted]: <CheckIcon size={size} />,
  };

  return scoreStatusIcons[status];
}

export function ScoreStatusDisplay({ status }) {
  return <span>{toTitleCase(status)}</span>;
}

export const PointsTitle = () => <Trans>Points</Trans>;

export const GradeTitle = ({ modeId }) => {
  const isSakuraMode = [18, 23].includes(modeId);
  return isSakuraMode ? <Trans>Stage</Trans> : <Trans>Grade</Trans>;
};

export const ScoreTitle = ({ modeId }) => {
  const isEasyMode = [17, 22].includes(modeId);
  return isEasyMode ? <Trans>Hanabi</Trans> : <Trans>Score</Trans>;
};

export const LevelTitle = ({ modeId }) => {
  const isSakuraMode = [18, 23].includes(modeId);
  return isSakuraMode ? <Trans>Clear %</Trans> : <Trans>Level</Trans>;
};

export const TimeTitle = () => <Trans>Time</Trans>;
