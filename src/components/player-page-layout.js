import React from 'react';
import PlayerBio from './player-bio';

import './player-page-layout.css';

export default function PlayerPageLayout({
  renderHeader,
  playerName,
  children,
}) {
  return (
    <div className="player-page">
      <div className="player-page__aside">
        {playerName && <PlayerBio playerName={playerName} />}
      </div>
      <div className="player-page__main">
        {renderHeader && renderHeader()}
        {children}
      </div>
    </div>
  );
}
