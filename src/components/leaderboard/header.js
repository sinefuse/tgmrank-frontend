import React from 'react';
import objstr from 'obj-str';
import IconGrouped from '../../icons/grouped';
import IconUngrouped from '../../icons/ungrouped';

export default function LeaderboardHeader(props) {
  return (
    <>
      {props.isFilterOpen && <div>Filter...</div>}
      <header className="leaderboard-header">
        <h2>
          {props.isFilterOpen ? 'Filtered' : 'Full'} results{' '}
          <small>({props.scores.length})</small>
        </h2>
        {hasGroups && (
          <>
            <button className="push-right" onClick={() => setGrouped(true)}>
              <IconGrouped
                className={objstr({
                  grouped: true,
                  icon: true,
                  'icon--active': isGrouped,
                })}
              />
            </button>
            <button onClick={() => setGrouped(false)}>
              <IconUngrouped
                className={objstr({
                  ungrouped: true,
                  icon: true,
                  'icon--active': !isGrouped,
                })}
              />
            </button>
          </>
        )}
      </header>
    </>
  );
}
