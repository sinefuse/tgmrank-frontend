import React, { cloneElement } from 'react';
import './media.css';

export default function Media({ item, children }) {
  return (
    <div className="media">
      {cloneElement(item, {className: "media__item"})}
      <div className="media__content">{children}</div>
    </div>
  );
}
