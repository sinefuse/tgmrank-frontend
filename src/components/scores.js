import React, { useState } from 'react';
import { Link } from '@reach/router';
import { Trans } from '@lingui/macro';
import { useTransition, animated } from 'react-spring';
import getStats from './leaderboard/stats';
import ModeName from './mode-name';
import Cell from './cell';
import { leaderboardPath } from '../hooks/use-games';

export default function PlayerScores({ game, mode, scores }) {
  const stats = getStats({ mode });
  const [expanded, setExpand] = useState(false);

  const extraScores = expanded ? scores.slice(1) : [];

  //TODO Make time shorter if there are fewer items.
  const transitions = useTransition(extraScores, item => item.scoreId, {
    from: { marginTop: '-48px' },
    enter: { marginTop: '0px' },
    leave: { marginTop: '-48px' },
    config: { mass: 1, tension: 140, friction: 26 },
  });

  function toggleExpand() {
    if (expanded) {
      setExpand(false);
    } else {
      setExpand(true);
    }
  }

  return (
    <>
      <h3>
        <Link to={leaderboardPath({ game, mode })}>
          <ModeName {...mode} />
        </Link>
      </h3>
      <div className="leaderboard leaderboard--expandable">
        <Cell stats={stats} score={scores[0]} />
        {transitions.map(({ item, key, props }, index) => (
          <animated.div
            key={key}
            className="leaderboard__item--animated"
            style={{ zIndex: -index - 1, ...props }}
          >
            <Cell stats={stats} score={item} />
          </animated.div>
        ))}
      </div>
      {scores?.length > 1 &&
        (expanded ? (
          <button className="leaderboard__expand" onClick={toggleExpand}>
            <Trans>Hide score history</Trans>{' '}
            <div className="badge">{scores.length - 1}</div>
          </button>
        ) : (
          <button className="leaderboard__expand" onClick={toggleExpand}>
            <Trans>Show score history</Trans>{' '}
            <div className="badge">{scores.length - 1}</div>
          </button>
        ))}
    </>
  );
}
