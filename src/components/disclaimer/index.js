import React from 'react';
import { Trans } from '@lingui/macro';
import { ContactInfo } from '../../contact';

import './styles.css';

export function ExistingAccountDisclaimer() {
  return (
    <div className="disclaimer">
      <Trans>
        Already see your name or scores here? Contact an admin via{' '}
        <a href={ContactInfo.discord}>Discord</a> or{' '}
        <a href={`mailto:${ContactInfo.email}`}>email</a>
      </Trans>
    </div>
  );
}
