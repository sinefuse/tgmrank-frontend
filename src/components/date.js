import React from 'react';
import { i18n } from '@lingui/core';

export default function LongDate({ children }) {
  const date = new Date(children);
  const options = { year: 'numeric', month: 'long', day: 'numeric' };
  return (
    <time dateTime={new Date(date).toISOString()}>
      {i18n.date(date, options)}
    </time>
  );
}

export function LongDateTime({ children }) {
  const date = new Date(children);
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  };
  return (
    <time dateTime={new Date(date).toISOString()}>
      {i18n.date(date, options)}
    </time>
  );
}
