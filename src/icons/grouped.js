import React from 'react';

export default function IconGrouped(props) {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <g fill="none">
        <path
          className="grouped__stroke"
          d="M2 1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h20a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm0 13a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h20a1 1 0 0 0 1-1v-7a1 1 0 0 0-1-1H2z"
          strokeWidth="2"
        />
        <path
          className="grouped__fill"
          d="M5 3h14a1 1 0 0 1 0 2H5a1 1 0 1 1 0-2zm0 13h14a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm0 3h14a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zM5 6h14a1 1 0 0 1 0 2H5a1 1 0 1 1 0-2z"
        />
      </g>
    </svg>
  );
}
