import React from 'react';
import { Helmet } from 'react-helmet-async';
import useSWR from 'swr';
import TgmRank from '../../tgmrank-api';
import Layout from '../../components/layout';
import { useAuth } from '../../hooks/use-auth';
import { isAdmin } from '../../contexts/user';
import { useToastManager } from '../../hooks/use-toast-manager';
import { useForm } from 'react-hook-form';
import { toTitleCase } from '../../components/string-utilities';

function CreateUserForm() {
  const toastManager = useToastManager();
  const { register, handleSubmit, reset } = useForm();

  async function createUser(data) {
    try {
      await TgmRank.createUser(data.username);
      toastManager.addSuccess(`Created User "${data.username}"`);
    } catch (e) {
      toastManager.addError(e.errorString ?? e);
    } finally {
      reset();
    }
  }

  return (
    <form className="add-score" onSubmit={handleSubmit(createUser)}>
      <div className="add-score__content">
        <div className="form-section">
          <div className="form-section__info">
            <h4>Create Legacy User</h4>
            <p>Add a user that will need to be claimed in the future</p>
          </div>
          <div className="form-section__form">
            <div className="form-group">
              <label>Username</label>
              <input
                name="username"
                type="text"
                placeholder="Username"
                ref={register({ required: true })}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="add-score__footer">
        <button className="button">Create User</button>
      </div>
    </form>
  );
}

function ClaimAccountForm() {
  const toastManager = useToastManager();
  const { register, handleSubmit, reset } = useForm();

  const { data: allPlayers } = useSWR(TgmRank.getAllPlayersUrl());

  async function claimAccount(data) {
    try {
      await TgmRank.claimAccount(data.username, data.email);
      toastManager.addSuccess(
        `Claimed User "${data.username}" using email "${data.email}"`,
      );
    } catch (e) {
      toastManager.addError(e);
    } finally {
      reset();
    }
  }

  return (
    <form className="add-score" onSubmit={handleSubmit(claimAccount)}>
      <div className="add-score__content">
        <div className="form-section">
          <div className="form-section__info">
            <h4>Claim Account</h4>
            <p>Update a legacy account&apos;s email address</p>
          </div>
          <div className="form-section__form">
            <div className="form-group">
              <label>Username</label>
              <select
                className="select"
                name="username"
                ref={register({ required: true })}
              >
                <option value="">Select player</option>
                {allPlayers?.map(p => (
                  <option key={p.playerId} value={p.playerName}>
                    {p.playerName}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label>Email Address</label>
              <input
                name="email"
                type="email"
                placeholder="Email"
                ref={register({ required: true })}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="add-score__footer">
        <button className="button">Claim Account</button>
      </div>
    </form>
  );
}

function UpdatePlayerRole() {
  const toastManager = useToastManager();
  const { register, handleSubmit, reset } = useForm();

  const { data: allPlayers } = useSWR(TgmRank.getAllPlayersUrl());
  const { data: playerRoles } = useSWR(TgmRank.getPlayerRolesUrl());

  async function updateRole(data) {
    try {
      await TgmRank.updatePlayerRole(data.userId, data.role);
      const username = allPlayers?.find(p => p.playerId === Number(data.userId))
        .playerName;

      toastManager.addSuccess(
        `Updated User "${username}" to role "${data.role}"`,
      );
    } catch (e) {
      toastManager.addError(e);
    } finally {
      reset();
    }
  }

  return (
    <form className="add-score" onSubmit={handleSubmit(updateRole)}>
      <div className="add-score__content">
        <div className="form-section">
          <div className="form-section__info">
            <h4>Update Player Role</h4>
            <p>Ban/Unban users</p>
          </div>
          <div className="form-section__form">
            <div className="form-group">
              <label>Username</label>
              <select
                className="select"
                name="userId"
                ref={register({ required: true })}
              >
                <option value="">Select player</option>
                {allPlayers?.map(p => (
                  <option key={p.playerId} value={p.playerId}>
                    {p.playerName}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label>Role</label>
              <select
                className="select"
                name="role"
                ref={register({ required: true })}
              >
                {playerRoles?.map(r => (
                  <option key={r} value={r}>
                    {toTitleCase(r)}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
      <div className="add-score__footer">
        <button className="button">Update Role</button>
      </div>
    </form>
  );
}

function MiscellaneousStuff() {
  const toastManager = useToastManager();

  async function flushCache(event) {
    event.preventDefault();
    try {
      await TgmRank.flushApiCache();
      toastManager.addSuccess(`Flushed API cache`);
    } catch (e) {
      toastManager.addError(e);
    }
  }

  return (
    <div className="add-score">
      <div className="add-score__content">
        <div className="form-section">
          <div className="form-section__info">
            <h4>Miscellaneous</h4>
            <p>Some stuff</p>
          </div>
          <div className="form-section__form">
            <div className="form-group">
              <label>Flush API Cache</label>
              <button className="button" onClick={flushCache}>
                Flush
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default function AdminPage() {
  useAuth(isAdmin, '/not-found');
  return (
    <Layout>
      <Helmet>
        <title>Admin Panel</title>
      </Helmet>
      <CreateUserForm />
      <ClaimAccountForm />
      <UpdatePlayerRole />
      <MiscellaneousStuff />
    </Layout>
  );
}
