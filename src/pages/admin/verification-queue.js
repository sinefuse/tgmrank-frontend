import React, { useState } from 'react';

import { Helmet } from 'react-helmet-async';
import useSWR from 'swr';
import Wrapper from '../../components/cell-wrapper';
import ActivityCell from '../../components/cell/activity';
import Dialog from '../../components/dialog';
import Layout from '../../components/layout';
import { canVerifyScores } from '../../contexts/user';
import { useAuth } from '../../hooks/use-auth';
import TgmRank, { ScoreStatus } from '../../tgmrank-api';
import { Score } from '../score';

import './styles.css';

function Page({ page, statuses }) {
  const { data, error } = useSWR(
    TgmRank.getRecentActivityUrl({
      page: page,
      pageSize: 10,
      scoreStatuses: statuses,
    }),
  );

  const [scoreIdToShow, setScoreIdToShow] = useState(null);

  return (
    <div className="activity">
      {data?.map((item, index) => {
        return (
          <Wrapper
            key={index}
            onClick={() => setScoreIdToShow(item.score.scoreId)}
          >
            <ActivityCell item={item} showProfile={true} />
          </Wrapper>
        );
      })}
      <Dialog
        className="verification-queue__dialog"
        isOpen={scoreIdToShow != null}
        close={() => setScoreIdToShow(null)}
        aria-label="Score details dialog"
      >
        <Score scoreId={scoreIdToShow} />
      </Dialog>
    </div>
  );
}

export default function VerificationQueuePage() {
  useAuth(canVerifyScores, '/not-found');

  const [pageIndex, setPageIndex] = useState(1);
  const [statuses, setStatuses] = useState([ScoreStatus.Pending]);
  function onChangeStatus(event) {
    if (event.target.checked) {
      setStatuses(prevState => [...prevState, event.target.value]);
    } else {
      setStatuses(prevState =>
        prevState.filter(status => status !== event.target.value),
      );
    }
    setPageIndex(1);
  }

  function PageNavigationButtons() {
    return (
      <div className="pack flex-gap-1">
        <button
          className="button"
          onClick={() => setPageIndex(Math.max(pageIndex - 1, 1))}
        >
          Previous
        </button>
        <span>Page {pageIndex}</span>
        <button className="button" onClick={() => setPageIndex(pageIndex + 1)}>
          Next
        </button>
      </div>
    );
  }

  return (
    <Layout>
      <Helmet>
        <title>Verification Queue</title>
      </Helmet>
      <h1>Verification Queue</h1>
      <div className="pack pack--column flex-gap-column-1">
        <div className="pack flex-gap-1 no-select">
          {Object.entries(ScoreStatus).map(([k, v], index) => (
            <React.Fragment key={index}>
              <input
                type="checkbox"
                id={v}
                name="status"
                value={v}
                checked={statuses.includes(v)}
                onChange={onChangeStatus}
              />
              <label htmlFor={v}>{k}</label>
            </React.Fragment>
          ))}
        </div>
        <PageNavigationButtons />
        <Page page={pageIndex} statuses={statuses} />
        <div style={{ display: 'none' }}>
          <Page page={pageIndex + 1} statuses={statuses} />
        </div>
        <PageNavigationButtons />
      </div>
    </Layout>
  );
}
