import React from 'react';
import { Helmet } from 'react-helmet-async';
import LongDate from '../../components/date';
import Layout from '../../components/layout';
import Link from '../../components/link';

import FrameRateExample from '../../../images/mame_tap_framerate_example.png';

export default function ProofPolicyPage() {
  return (
    <Layout>
      <Helmet>
        <title>Proof Policy</title>
      </Helmet>
      <div className="proof-policy">
        <h1>Proof Policy</h1>
        <p>
          Last Updated On:{' '}
          <strong>
            <LongDate>2021-10-29T04:56:12+0000</LongDate>
          </strong>
        </p>
        <p>
          This document outlines the full list of policies that must be followed
          in order to be given a rank on theabsolute.plus. This policy first
          took effect on{' '}
          <strong>
            <LongDate>2021-10-29T04:56:12+0000</LongDate>
          </strong>{' '}
          and there is a two-week grace period where the old proof methods will
          still be accepted. Runs submitted before{' '}
          <strong>
            <LongDate>2021-11-12T15:31:06Z</LongDate>
          </strong>{' '}
          will not be removed from theabsolute.plus unless they are determined
          to be fraudulent. That is, old runs will not be held to the current
          proof standards.
        </p>
        <h2>Proving Runs</h2>
        <p>
          To maintain the integrity of the leaderboards on theabsolute.plus,
          there are requirements on the way a run should be exhibited. The
          higher the echelon a run appears to be in, the more strict the proof
          requirements are. The following is a complete list of rules that need
          to be followed when submitting to the leaderboards, otherwise the run
          will be rejected by a moderator.
        </p>
        <h3>For &quot;at home&quot; setups</h3>
        <ul>
          <li>
            Any run that will be awarded 60 or more points on an individual
            leaderboard requires video proof.
          </li>
          <li>
            Any run that is awarded the Grand Master grade requires video proof.
          </li>
          <li>
            The run must be played within one of the three games: &quot;Tetris
            The Grand Master&quot;, &quot;Tetris The Absolute The Grand Master 2
            Plus&quot;, or &quot;Tetris The Grand Master 3
            Terror-Instinct&quot;.
          </li>
          <li>
            Any run submitted that is NOT a personal best will not be as
            scrutinized and does not require video proof.
          </li>
          <li>
            If the claim of an &quot;Orange Line&quot; is made, then the orange
            line on the in-game leaderboard needs to be displayed in a video or
            picture.
          </li>
          <li>
            If requested, the player must be able to demonstrate that the game
            was played at an accurate frame-rate: 60 frames per second for TGM1
            and TGM3, and 61.68 frames per second for TGM2+.
            <figure>
              <img
                src={FrameRateExample}
                alt="Image of TGM2+ running in MAME with the Game Information modal showing 61.68... frames per second"
              />
              <figcaption>
                For example, &quot;Tetris The Grand Master 2 Plus&quot; must be
                run at 61.68 frames per second.
              </figcaption>
            </figure>
          </li>
        </ul>
        <h3>For &quot;arcade&quot; setups</h3>
        <ul>
          <li>
            If you are unable to record at an arcade, then a picture clearly
            showing the time the run finished (as opposed to the Mastering
            Time), and a picture of the arcade cabinet is required as proof.
          </li>
          <li>
            If the claim of an &quot;Orange Line&quot; is made, then the orange
            line on the in-game leaderboard needs to be displayed in a picture.
          </li>
        </ul>
        <h3>Other</h3>
        <ul>
          <li>
            Any run not falling into one of the previous categories requires
            picture proof, either of the game screen taken with some form of
            screen capture, or a picture taken of the monitor.
          </li>
        </ul>
        <h2>Proof Standards</h2>
        <h3>Video</h3>
        <ul>
          <li>The video has a quality of at least 360p</li>
          <li>
            The video frame rate is at least 30 fps if captured directly through
            a capture card or screen recording, or the standard frame rate for
            VHS/DVD recording of the country recorded in.
          </li>
          <li>
            If captured from a hand camera, such as a phone, then it needs to
            clearly show the game play at least 90% of the time, and clearly
            show the ending time of the run. Any invisible gameplay must be
            shown in full.
          </li>
          <li>
            No cropping the game screen unnecessarily. For example, some overlap
            with a stream overlay is acceptable, but the other player’s field
            needs to at the very least be partially visible if playing in
            two-player mode.
          </li>
          <li>
            The entire run needs to be in the video, from no later than
            &quot;Ready&quot; until at least the &quot;Game Over&quot; screen or
            &quot;Continue&quot; screen.
          </li>
        </ul>
        <h3>Picture</h3>
        <ul>
          <li>
            Pictures coming from a screen capture may not be cropped and must
            display the entire game, but like with videos, a small overlap with
            a stream overlay is acceptable.
          </li>
          <li>The picture must clearly show the time the run finished.</li>
          <li>
            If taking pictures in an arcade, be respectful of other
            people&apos;s privacy.
          </li>
        </ul>
        <h2>Proof Challenges</h2>
        <p>
          If a player is suspected of cheating, then the leaderboard moderators
          have the right to challenge a player on their run and request
          additional proof that the player is capable of such a run. This is
          likely to be in the form of doing another run with both the game
          screen and the player&apos;s hands in the video.
        </p>
        <p>
          The additional proof only needs to demonstrate your stacking and
          general speed. It does not need to be a new personal best, it just
          needs to demonstrate your general skill. It does need to be done as
          soon as possible, but the given time limit will be discussed on a per
          challenge basis for each player&apos;s specific situation.
        </p>
        <p>
          Once this video is submitted, the Moderation Team will decide if the
          player who submitted the additional proof could have played the
          original video.
        </p>
        <h2>Cheating</h2>
        <p>
          Any player caught cheating and hence breaking the integrity of the
          leaderboard will be permanently banned from the leaderboard and their
          scores revoked with no recourse. A non-exhaustive list of examples of
          cheating follows:
        </p>
        <ul>
          <li>Trying to pass a TAS run as a run played by themselves.</li>
          <li>
            Playing with a predetermined seed if the mode does not allow it
            (Sakura being the only mode with a fixed seed so far).
          </li>
          <li>
            Faking what frame rate the run is being played on. Honest mistakes
            are okay, just do not try to pass off an incorrect frame rate as
            correct. Own the mistake and adjust your setup.
          </li>
          <li>
            Using the player two field to display the play field in an invisible
            portion of the game.
          </li>
          <li>
            Splicing a video of a main game and an invisible portion of the game
            together.
          </li>
        </ul>
        <h2>theabsolute.plus Moderators</h2>
        <p>
          The moderators are here to resolve disputes related to rankings and to
          update and clarify any of theabsolute.plus&apos; rules should that
          need to happen.
        </p>
        <h3>Members</h3>
        <ul>
          <li>
            <a href="/player/colour_thief">colour_thief</a>
          </li>
          <li>
            <a href="/player/JBroms">JBroms</a>
          </li>
          <li>
            <a href="/player/MHL">MHL</a>
          </li>
          <li>
            <a href="/player/switchpalacecorner">switchpalacecorner</a>
          </li>
        </ul>
        <h3>How to contact us</h3>
        <p>
          If you have any questions about theabsolute.plus&apos; proof policy,
          contact information can be found on the <Link to="/about">about</Link>{' '}
          page.
        </p>
        <h3>Removal of a moderator</h3>
        <p>
          Should a moderator be found to be lacking integrity in their decision
          making, they will be removed from their role as a moderator. Any
          discussion surrounding the removal of a moderator will happen in a
          public setting.
        </p>
      </div>
    </Layout>
  );
}
