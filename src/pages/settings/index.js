import { t, Trans } from '@lingui/macro';
import { useLingui } from '@lingui/react';
import { Link } from '@reach/router';
import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useForm } from 'react-hook-form';
import useSWR, { mutate } from 'swr';
import root from 'window-or-global';
import { LongDateTime } from '../../components/date';
import Flag from '../../components/flag';
import Layout from '../../components/layout';
import { Panels, Tabs, TabsContainer } from '../../components/tabs';
import { useUserLocations } from '../../contexts/locations';
import { useUser } from '../../contexts/user';
import { useAuth } from '../../hooks/use-auth';
import { useToastManager } from '../../hooks/use-toast-manager';
import TgmRank from '../../tgmrank-api';

function SecurityForms() {
  const toastManager = useToastManager();
  const { verifyLogin } = useUser();
  const [isProcessing, setProcessing] = useState(false);
  const { register, handleSubmit, errors, reset } = useForm();

  async function onSubmit(data) {
    try {
      if (data.newPassword && data.newPassword !== data.newPasswordConfirm) {
        toastManager.addError('Passwords do not match');
        return;
      }

      setProcessing(true);
      await TgmRank.updatePlayerPassword(data.oldPassword, data.newPassword);

      toastManager.addSuccess(
        'Your password has been changed. All other sessions have been logged out.',
      );

      reset();
    } catch (e) {
      toastManager.addError(e.errorString ?? e);
    } finally {
      setProcessing(false);
      await verifyLogin();
    }
  }

  return (
    <form className="add-score" onSubmit={handleSubmit(onSubmit)}>
      <div className="add-score__content">
        <div className="form-section">
          <div className="form-section__info">
            <h3>
              <Trans>Change Password</Trans>
            </h3>
            <p></p>
          </div>
          <div className="form-section__form">
            <div className="form-group">
              <label htmlFor="oldPassword">
                <Trans>Old Password</Trans>
              </label>
              <input
                id="oldPassword"
                name="oldPassword"
                type="password"
                ref={register({ required: true, min: 8 })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="newPassword">
                <Trans>New Password</Trans>
              </label>
              <input
                id="newPassword"
                name="newPassword"
                type="password"
                ref={register({ required: true, min: 8 })}
              />
            </div>
            <div className="form-group">
              <label htmlFor="newPasswordConfirm">
                <Trans>New Password (again)</Trans>
              </label>
              <input
                id="newPasswordConfirm"
                name="newPasswordConfirm"
                type="password"
                ref={register({ required: true, min: 8 })}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="add-score__footer">
        <button className="button" disabled={isProcessing}>
          {isProcessing ? (
            <Trans>Changing Password...</Trans>
          ) : (
            <Trans>Change Password</Trans>
          )}
        </button>
      </div>
    </form>
  );
}

function LocationSelector() {
  const { i18n } = useLingui();

  const toastManager = useToastManager();
  const { user, verifyLogin } = useUser();
  const [isProcessing, setProcessing] = useState(false);
  const { register, handleSubmit, reset } = useForm();

  const { locations: locationData } = useUserLocations();

  useEffect(() => {
    reset({
      location: user?.location,
    });
  }, [user]);

  async function onSubmit(data) {
    try {
      setProcessing(true);
      await TgmRank.updatePlayerLocation(data.location);
      toastManager.addSuccess('Your location has been updated!');
    } catch (e) {
      toastManager.addError('An error occurred while updating your location.');
    } finally {
      await verifyLogin();
      setProcessing(false);
    }
  }

  return (
    <form className="form-section" onSubmit={handleSubmit(onSubmit)}>
      <div className="form-section__info">
        <h3>
          <Trans>Location</Trans> <Flag code={user?.location} />
        </h3>
      </div>
      <div className="form-section__form">
        <div className="form-group">
          <select className="select" name="location" ref={register}>
            <option value="">{i18n._(t`Select Location`)}</option>
            <option value="">{i18n._(t`[Unset]`)}</option>
            {locationData?.map(location => (
              <option key={location.alpha2} value={location.alpha2}>
                {location.name}
              </option>
            ))}
          </select>
          <div className="add-score__footer">
            <button className="button" disabled={isProcessing}>
              <Trans>Update Location</Trans>
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

function ChangeUsernameForm() {
  const toastManager = useToastManager();
  const { user, verifyLogin } = useUser();
  const [isProcessing, setProcessing] = useState(false);
  const { register, handleSubmit } = useForm();

  const { data: usernameHistory } = useSWR(
    user?.userId != null
      ? TgmRank.getPlayerUsernameHistoryUrl(user?.userId)
      : null,
  );

  const usernameUpdateTimeoutInMs = 5184000000; // 60 Days
  const canUpdateUsername =
    Date.now() >
    Date.parse(usernameHistory?.[0].updatedAt) + usernameUpdateTimeoutInMs;

  async function onSubmit(data) {
    try {
      setProcessing(true);
      await TgmRank.updatePlayerUsername(data.username);
      mutate(TgmRank.getPlayerUsernameHistoryUrl(user.userId).toString());
      toastManager.addSuccess('Your username has been updated!');
    } catch (e) {
      toastManager.addError(
        `An error occurred while updating your username: ${e.error}`,
      );
    } finally {
      await verifyLogin();
      setProcessing(false);
    }
  }

  return (
    <form className="form-section" onSubmit={handleSubmit(onSubmit)}>
      <div className="form-section__info">
        <h3>
          <Trans>Change Username</Trans>
          <p>
            <Trans>
              Note: You can only change your username once every 60 days. Be
              careful.
            </Trans>
          </p>
        </h3>
      </div>
      <div className="form-section__form">
        <div className="form-group">
          <label htmlFor="username">
            <Trans>New Username</Trans>
          </label>
          <input
            id="username"
            name="username"
            disabled={!canUpdateUsername}
            ref={register({ required: true, min: 8 })}
          />
          {usernameHistory?.length > 0 && (
            <>
              <p>
                <Trans>Username history</Trans>:
              </p>
              <ul>
                {usernameHistory.map((user, i) => (
                  <li key={i}>
                    <b>{user.username}</b> (
                    <LongDateTime>{user?.updatedAt}</LongDateTime>)
                  </li>
                ))}
              </ul>
            </>
          )}
          <div className="add-score__footer">
            <button
              className="button"
              disabled={!canUpdateUsername || isProcessing}
            >
              {!canUpdateUsername ? (
                <Trans>Cannot Update Username</Trans>
              ) : (
                <Trans>Update Username</Trans>
              )}
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

function ProfileForm() {
  const toastManager = useToastManager();
  const { user, verifyLogin } = useUser();
  const [isProcessing, setProcessing] = useState(false);
  const { register, handleSubmit, reset } = useForm();

  async function onSubmit(data) {
    try {
      setProcessing(true);

      let avatarFile = data?.avatarFile[0];
      if (avatarFile) {
        await TgmRank.uploadAvatar(avatarFile);
        toastManager.addSuccess('Your avatar has been updated!');
      }

      await verifyLogin();
    } catch (e) {
      toastManager.addError(e.errorString ?? e);
    } finally {
      setProcessing(false);
      reset();
    }
  }

  return (
    <div className="add-score">
      <div className="add-score__content">
        <ChangeUsernameForm />
        <form className="form-section" onSubmit={handleSubmit(onSubmit)}>
          <div className="form-section__info">
            <h3>
              <Trans>Avatar</Trans>
            </h3>
          </div>
          <div className="form-section__form">
            <div className="form-group">
              <label>
                <Trans>Current Avatar</Trans>
              </label>
              <br />
              <img
                width="150"
                src={TgmRank.getAvatarLink(user?.avatar)}
                alt={`${user?.username}'s Avatar`}
              />
            </div>
            <div className="form-group">
              <label>
                <Trans>New Avatar (100kb file size limit)</Trans>
              </label>
              <input
                name="avatarFile"
                type="file"
                ref={register({ required: true })}
                accept="image/bmp,image/gif,image/png,image/jpeg"
              />
            </div>
            <div className="add-score__footer">
              <button className="button" disabled={isProcessing}>
                {isProcessing ? (
                  <Trans>Updating Avatar...</Trans>
                ) : (
                  <Trans>Update Avatar</Trans>
                )}
              </button>
            </div>
          </div>
        </form>
        <LocationSelector />
      </div>
    </div>
  );
}

export default function SettingsPage() {
  useAuth();

  const { i18n } = useLingui();

  const tabs = new Map([
    [t`Profile`, '/settings/profile'],
    [t`Security`, '/settings/security'],
  ]);

  let selectedTab = Array.from(Array.from(tabs.values())).findIndex(
    v => v.toLowerCase() === root?.location?.pathname.toLowerCase(),
  );

  if (selectedTab === -1) {
    selectedTab = 0;
  }

  return (
    <Layout>
      <TabsContainer
        selectedTab={selectedTab}
        labels={Array.from(tabs.keys())}
        render={(context, activeTabLabel) => (
          <div>
            <Helmet>
              <title>User Settings - {i18n._(activeTabLabel)}</title>
            </Helmet>
            <header>
              <h1>
                <Trans>User Settings</Trans>
              </h1>
            </header>
            <Tabs
              context={context}
              tab={tabName => (
                <Link to={tabs.get(tabName)}>
                  <Trans id={tabName} />
                </Link>
              )}
            />
            <Panels context={context}>
              <ProfileForm />
              <SecurityForms />
            </Panels>
          </div>
        )}
      />
    </Layout>
  );
}
