export const localStorageWrapper = () =>
  typeof localStorage !== 'undefined' && localStorage !== null
    ? localStorage
    : null;
