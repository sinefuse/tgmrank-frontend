const options = {
  mode: 'cors',
  cache: 'no-cache',
  credentials: 'include',
  referrer: 'no-referrer',
};

export const get = url =>
  fetch(url, {
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    redirect: 'manual',
    ...options,
  });

export const post = (url, body, method = 'POST') =>
  fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    redirect: 'manual',
    body: JSON.stringify(body),
    ...options,
  });

export const formPost = (url, body) =>
  fetch(url, {
    method: 'POST',
    body: body,
    ...options,
  });

export async function fetcher(url) {
  const res = await get(url);
  const json = await res.json();

  if (!res.ok) {
    throw json;
  }

  return json;
}
